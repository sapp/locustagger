package nl.wur.ssb.locustagger;

import junit.framework.TestCase;
import nl.wur.ssb.HDT.HDT;

public class AppTest extends TestCase {

	public void testLocus() throws Exception {
		String[] args = new String[] { "-i", "./src/test/resources/H2.hdt", "-o", "./src/test/resources/galaxy_h2.hdt",
				"-prefix", "Another_" };
		App.main(args);
		HDT hdt = new HDT();
		hdt.hdt2rdf("./src/test/resources/galaxy_h2.hdt", "./src/test/resources/galaxy_locus.ttl");
	}

	public void testNoLocus() throws Exception {
		String[] args2 = new String[] { "-i", "./src/test/resources/noLocus.hdt", "-o",
				"./src/test/resources/galaxy_NoLocus.hdt", "-prefix", "MYOWN" };
		App.main(args2);
		HDT hdt = new HDT();
		hdt.hdt2rdf("./src/test/resources/galaxy_NoLocus.hdt", "./src/test/resources/galaxy_NoLocus.ttl");
	}
}

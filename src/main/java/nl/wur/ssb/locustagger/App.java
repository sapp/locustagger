package nl.wur.ssb.locustagger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.rdfhdt.hdt.exceptions.ParserException;

import nl.wur.ssb.HDT.HDT;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.data.Domain;

public class App {
	private static CommandLine arguments;
	private static RDFSimpleCon RDFResource;
	private static Domain domain;

	public static void main(String[] args) throws Exception {

		arguments = CommandParser.argsValidator(args);
		Logger.getRootLogger().setLevel(Level.OFF);

		String input = arguments.getOptionValue("input");

		File temp = File.createTempFile("turtle", ".tmp");
		HDT hdt = new HDT();
		hdt.hdt2rdf(input, temp.getAbsolutePath());
		input = temp.getAbsolutePath();
		// Current solution as HDT does not allow updates on the fly and it is
		// needed in this script...
		loadRDF();

		inferrer();

	}

	private static void loadRDF() throws Exception {
		String input = arguments.getOptionValue("input");

		System.out.println("HDT file detected");
		String temp = File.createTempFile("turtle", ".tmp").getAbsolutePath();
		HDT hdt = new HDT();
		hdt.hdt2rdf(input, temp);

		System.out.println("TEMP FILE:" + input);
		RDFResource = new RDFSimpleCon("file://" + temp + "{NT}");
		RDFResource.setNsPrefix("ssb", "http://csb.wur.nl/genome/");
		RDFResource.setNsPrefix("protein", "http://csb.wur.nl/genome/protein/");
		RDFResource.setNsPrefix("biopax", "http://www.biopax.org/release/bp-level3.owl#");
	}

	public static void inferrer() throws Exception {

		// Container for already existing locus tags
		List<String> locusContainer = new ArrayList<String>();

		// Creation of accession for contigs
		ScaffoldAccessions();

		String leadingZeros = totalGenes();

		// runquerytomap not available yet in HDT

		LinkedList<ResultLine> genesSAPP = RDFResource.runQueryToMap("getGenesSAPP.txt", false, "some par");

		RDFResource.setNsPrefix("ssb", "http://csb.wur.nl/genome/");
		List<String> hasLocus = new ArrayList<String>();

		for (ResultLine item : RDFResource.runQuery("getLocusTags.txt", false, "some par")) {
			char alphabet = '@';

			int begin = item.getLitInt("begin");
			int end = item.getLitInt("end");
			String locus = item.getLitString("locus");

			// Adding existing locus for later lookup
			locusContainer.add(locus);

			boolean locus_match = false;
			boolean sapp_match = false;
			int strandSAPP;
			int strandGenbank = begin < end ? 1 : -1;
			String geneSAPP = "";

			for (ResultLine item2 : genesSAPP) {
				int beginSAPP = item2.getLitInt("begin");
				int endSAPP = item2.getLitInt("end");
				geneSAPP = item2.getIRI("gene");

				strandSAPP = beginSAPP < endSAPP ? 1 : -1;

				if (strandGenbank == strandSAPP)
				// Same strand from now on...
				{
					// Exact match
					if (begin == beginSAPP && end == endSAPP) {
						locus_match = true;
						sapp_match = true;

						// System.out.println(geneSAPP + " " + locus);
						RDFResource.addLit(geneSAPP, "ssb:locus_tag", locus);

						// Locus information of exact match
						break;
					} else if (end == endSAPP && strandGenbank == strandSAPP)
					// Stop match position on same strand found
					{
						locus_match = true;
						sapp_match = true;
						// System.out.println(geneSAPP + " " + locus);
						RDFResource.addLit(geneSAPP, "ssb:locus_tag", locus);

						// Locus information of only stop exact match
						break;
					} else if (begin == beginSAPP && end != endSAPP)
					// Same start different end
					{
						System.out.println("Same start position but different end position?.. Should be impossible");
					}
					// Same strand but locus gene lies within the other gene
					else if (begin > beginSAPP && end < endSAPP) {
						locus_match = true;
						sapp_match = true;
						alphabet++;
						// System.out.println(locus + alphabet);
						RDFResource.addLit(geneSAPP, "ssb:locus_tag", locus);

					}
					// Same strand but other gene lies within the locus gene
					else if (beginSAPP > begin && endSAPP < end) {
						locus_match = true;
						sapp_match = true;
						alphabet++;
						// System.out.println(locus + alphabet);
						RDFResource.addLit(geneSAPP, "ssb:locus_tag", locus);

					}
				}
				// Different strand match
				else
				// if (strandGenbank != strandSAPP)
				{
					if (begin > beginSAPP && end < endSAPP) {
						locus_match = true;
						sapp_match = true;
						// Original Gene lies within this gene as Reverse
						// Complement
						alphabet++;
						// System.out.println(locus + alphabet);
						// Gene gets locus tag + alphabetic character as it is
						// in the same
						// region as the previous locustag
						RDFResource.addLit(geneSAPP, "ssb:locus_tag", locus);

					}
					// Same strand but other gene lies within the locus gene
					else if (beginSAPP > begin && endSAPP < end) {
						locus_match = true;
						sapp_match = true;
						// SAPP Gene lies within this gene as Reverse Complement
						alphabet++;
						// System.out.println(locus + alphabet);
						// Gene gets locus tag + alphabetic character as it is
						// in the same
						// region as the previous locustag
						RDFResource.addLit(geneSAPP, "ssb:locus_tag", locus + alphabet);

					}
				}
			}
			if (locus_match == false) {
				System.out.println("No Hit:\t" + locus + "\t" + begin + "\t" + end);
			}
			if (sapp_match == true) {
				hasLocus.add(geneSAPP);
				// System.out.println("No locustag for...");
			}
		}
		int index = 0;
		String prefix = arguments.getOptionValue("prefix");
		for (ResultLine item : RDFResource.runQueryToMap("getLeftOvers.txt", false, "some par")) {

			index++;
			// System.out.println(item);
			String geneNoLocus = item.getIRI("gene");
			// System.out.println(geneNoLocus);
			String locus = prefix + String.format(leadingZeros, index);
			// To prevent locus collision, if already exists increase by 1
			while (Arrays.asList(locusContainer).contains(locus)) {
				index++;
			}
			RDFResource.addLit(geneNoLocus, "ssb:locus_tag", locus);

		}
		save();
	}

	private static void save() throws IOException, ParserException {
		HDT hdt = new HDT();
		hdt.merge(RDFResource, arguments.getOptionValue("input"), arguments.getOptionValue("output"));
	}

	private static String totalGenes() throws Exception {
		for (ResultLine item : RDFResource.runQueryToMap("getTotalGenes.txt", false, "some par")) {

			return "%0" + String.valueOf(item.getLitInt("total")).length() + "d";
		}
		return "%05d";
	}

	private static String totalContigs() throws Exception {
		for (ResultLine item : RDFResource.runQueryToMap("getTotalContigs.txt", false, "some par")) {

			return "%0" + String.valueOf(item.getLitInt("total")).length() + "d";
		}
		return "%05d";
	}

	private static void ScaffoldAccessions() throws Exception {
		int contigCounter = 0;
		String leadingZeros = totalContigs();
		RDFResource.runUpdateQuery("removeAccessionFromContigs.txt");
		for (ResultLine item : RDFResource.runQuery("getDnaObjects.txt", false)) {

			String accession = "Unknown_";
			String header = item.getLitString("header");
			String dna = item.getIRI("dna");
			contigCounter++;

			if (arguments.hasOption("scaffold") && !arguments.getOptionValue("scaffold").equals("")) {
				String accessionPrefix = arguments.getOptionValue("scaffold");
				accession = accessionPrefix + String.format(leadingZeros, contigCounter);
			} else if (header != null) {
				String subheader = header.replace(">", "").split(" ")[0];
				if (subheader.length() > 15) {
					// LAST 15 characters of first part of subheader
					accession = subheader.substring(subheader.length() - 15);
				} else {
					accession = subheader;
				}
			}
			RDFResource.addLit(dna, "ssb:accession", accession);

		}
	}

	// private void argsValidator() throws Exception {
	// if (!Arrays.asList(args).contains("-input"))
	// throw new Exception("No input file given using -input");
	// if (!Arrays.asList(args).contains("-format"))
	// throw new Exception("No format type given using -format");
	// if (!Arrays.asList(args).contains("-output"))
	// throw new Exception("No output file given using -output");
	// }
}

SELECT ?sha384 ?dna ?sequence ?table ?header
WHERE {
	?dna a ssb:DnaObject .
	?dna ssb:sequence ?sequence .
	?dna ssb:sha384 ?sha384 .
	OPTIONAL { ?dna ssb:transl_table ?table . }
	OPTIONAL { ?dna ssb:header ?header . }
	} ORDER BY ?header
DELETE { ?gene ssb:locus_tag ?locus }
WHERE {
	?gene a ssb:Gene .
	?gene ssb:locus_tag ?locus .
	}